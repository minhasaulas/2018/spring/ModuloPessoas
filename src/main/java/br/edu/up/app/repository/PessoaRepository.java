package br.edu.up.app.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.edu.up.app.dominio.Pessoa;

@RepositoryRestResource(path = "pessoas", collectionResourceRel = "pessoas", excerptProjection = Pessoa.class)
public interface PessoaRepository extends CrudRepository<Pessoa, Long> {

	Pessoa findByNome(@Param("nome") String nome);
	//Pessoa findByCPF(@Param("cpf") String cpf);
}
package br.edu.up.app.dominio;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "cliente", types = Pessoa.class)
public interface Cliente {

	@Value("#{target.id}")
	long getIdPessoa();
	String getNome();
}

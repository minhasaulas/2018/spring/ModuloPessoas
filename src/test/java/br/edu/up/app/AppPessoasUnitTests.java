package br.edu.up.app;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.edu.up.app.dominio.Pessoa;
import br.edu.up.app.repository.PessoaRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AppPessoasUnitTests {
	
	@Autowired
	public PessoaRepository repository;
	
	//@Test
	public void listarPessoasTest() {
		repository.findAll().forEach(System.out::println); 
	}

	//@Test
	public void incluirPessoasTest() {
		
		Pessoa p1 = new Pessoa("Ana", "Pereira");
		repository.save(p1);
		
		Pessoa p2 = new Pessoa("Patrícia", "Santos");
		repository.save(p2);	
		
		Pessoa p3 = new Pessoa("Joana", "Silveira");
		repository.save(p3);
		
	}
	
	//@Test
	public void buscarPessoaPorNomeTest() {
		
		Pessoa p1 = repository.findByNome("Pedro");
		System.out.println(p1);
		
	}
	
	@Test
	public void atualizarPessoaTest() {
		
		Pessoa p1 = repository.findByNome("Pedro");
		System.out.println(p1);
		p1.setSobrenome(p1.getSobrenome() + "+");
		repository.save(p1);
		p1 = repository.findByNome("Pedro");
		System.out.println(p1);
		
	}
	
	//@Test
	public void apagarPessoaTest() {
		
		repository.deleteById(6L);
		repository.findAll().forEach(System.out::println); 
	}

}

package br.edu.up.app;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.mvc.TypeConstrainedMappingJackson2HttpMessageConverter;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import br.edu.up.app.dominio.Pessoa;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WsPessoasUnitTests {

	private static String PESSOAS_ENDPOINT = "http://localhost:8084/pessoas";

	private ClientHttpRequestFactory getHttpClientFactory() {
	    int timeout = 5000;
	    HttpComponentsClientHttpRequestFactory httpClient
	      = new HttpComponentsClientHttpRequestFactory();
	    httpClient.setConnectTimeout(timeout);
	    return httpClient;
	}
	
	//@Test
	public void buscarPessoaPorIDJSONTest() {
		
		//Retorno em JSON
		RestTemplate rest = new RestTemplate();
		ResponseEntity<String> response = rest.getForEntity(PESSOAS_ENDPOINT + "/1", String.class);
		assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

	}
	
	@Autowired
    @Qualifier("halJacksonHttpMessageConverter")
	private TypeConstrainedMappingJackson2HttpMessageConverter converter;
	
	public RestTemplate getRestTemplate() {
		RestTemplate rest = new RestTemplate();
		List<HttpMessageConverter<?>> converters = rest.getMessageConverters();
		converters.add(0, converter);
		rest.setMessageConverters(converters);
		return rest;
	}
	
	@Test
	public void listarPessoasTest() {
		
		RestTemplate rest = getRestTemplate();
		ParameterizedTypeReference<PagedResources<Pessoa>> type = new ParameterizedTypeReference<PagedResources<Pessoa>>() {};
		ResponseEntity<PagedResources<Pessoa>> response = rest.exchange(PESSOAS_ENDPOINT, HttpMethod.GET, null, type);
		PagedResources<Pessoa> pResource = response.getBody();
		List<Pessoa> lista = new ArrayList<>(pResource.getContent());
		
		for (Pessoa pessoa : lista) {
			System.out.println(pessoa); 
		}
	}
	
	//@Test
	public void buscarPessoaPorIDPOJOTest() {
		
		//Retorno como objeto
		RestTemplate rest = new RestTemplate();
		Pessoa pessoa = rest.getForObject(PESSOAS_ENDPOINT + "/1", Pessoa.class);
		assertThat(pessoa.getNome(), notNullValue());
		assertThat(pessoa.getId(), is(1L));

	}
	
	//@Test
	public void incluirPessoaTest() {
		
		ClientHttpRequestFactory factory = getHttpClientFactory();
		RestTemplate rest = new RestTemplate(factory);
		 
		HttpEntity<Pessoa> request = new HttpEntity<>(new Pessoa("Antonio", "Lima"));
		Pessoa pessoa = rest.postForObject(PESSOAS_ENDPOINT, request, Pessoa.class);
		
		assertThat(pessoa, notNullValue());
		assertThat(pessoa.getNome(), is("Antonio"));
	}
	
	//@Test
	public void atualizarPessoaTest() {
		
		//Primeiro inclui uma pessoa
		RestTemplate rest = new RestTemplate();
		HttpEntity<Pessoa> requestCreate = new HttpEntity<>(new Pessoa("Teste", "Atualizar"));
		ResponseEntity<Pessoa> response = rest.exchange(PESSOAS_ENDPOINT, HttpMethod.POST, requestCreate, Pessoa.class);
		  
		assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
		  
		Pessoa pCriada = response.getBody();
		  
		assertThat(pCriada, notNullValue());
		assertThat(pCriada.getNome(), is("Teste"));
		
		//Depois atualiza
		pCriada.setSobrenome(pCriada.getSobrenome() + "+");

		String url = PESSOAS_ENDPOINT+ '/' + pCriada.getId();
		HttpEntity<Pessoa> requestUpdate = new HttpEntity<>(pCriada);
		rest.exchange(url, HttpMethod.PUT, requestUpdate, Void.class);
		
	}
	
	
	//@Test
	public void excluirPessoaTest() {
		
		//Primeiro inclui uma pessoa
		RestTemplate rest = new RestTemplate();
		HttpEntity<Pessoa> requestCreate = new HttpEntity<>(new Pessoa("Teste", "Excluir"));
		ResponseEntity<Pessoa> response = rest.exchange(PESSOAS_ENDPOINT, HttpMethod.POST, requestCreate, Pessoa.class);
		  
		assertThat(response.getStatusCode(), is(HttpStatus.CREATED));
		  
		Pessoa pCriada = response.getBody();
		  
		assertThat(pCriada, notNullValue());
		assertThat(pCriada.getNome(), is("Teste"));
		
		//Depois exclui
		String url = PESSOAS_ENDPOINT+ '/' + pCriada.getId();
		rest.delete(url);
		
	}
}
